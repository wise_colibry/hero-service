package main

import (
	"log"
	"os"
)

func main() {
	if err := Doit(); err != nil {
		log.Fatal(err)
	}
	os.Exit(0)
}

func Doit() error {

}
