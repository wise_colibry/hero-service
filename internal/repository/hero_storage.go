package memory

import (
	"time"
)

// Hero type for storage
type Hero struct {
	CreatedAt time.Time
	UpdatedAt time.Time
	Result    struct {
		Data struct {
			Heroes []struct {
				Id        int
				Name      string
				Str       int
				Strgain   float64
				Agi       int
				Agigain   float64
				Intel     int
				Intelgain float64
				Armor     float64
				Mresist   int
			}
		}
	}
}
