package transfer

import (
	"context"
	"hero-service/internal/common/server"
	"hero-service/internal/domain"
	"log"
	"net/http"
)

// HeroService simple hero service
type HeroService interface {
	GetHero(ctx context.Context, id string) (*domain.Hero, error)
	CountHeroes(ctx context.Context) (int, error)
	CreateUpdateTheHero(ctx context.Context, hero *domain.Hero) error
}

// HttpServer for heroes
type HttpServer struct {
	service HeroService
}

// Create a new Http server for heroes
func NewHttpServer(service HeroService) HttpServer {
	return HttpServer{
		service: service,
	}
}

// UploadHeroes read json heroe from file.json and do the create/update in database
func (h HttpServer) UploadHeroes(w http.ResponseWriter, r *http.Request) {
	log.Println("uploading heroes")

	heroChan := make(chan Hero)
	errorChan := make(chan error)
	finishedChan := make(chan struct{})

	go func() {
		err := readHeroes(r.Context(), r.Body, heroChan)
		if err != nil {
			errorChan <- err
		} else {
			finishedChan <- struct{}{}
		}
	}()

	counter := 0
	for {
		select {
		case hero := <-heroChan:
			counter++
			log.Printf("[%d] hero is received: %+v", counter, hero)
			d, err := heroToDomainViaHttp(&hero)
			if err != nil {
				server.BadRequest("hero-domain", err, w, r)
				return
			}
			if err := h.service.CreateUpdateTheHero(r.Context(), d); err != nil {
				server.RespondWithError(err, w, r)
				return
			}

		case <-finishedChan:
			log.Printf("readed all heroes")
			server.RespondOK(map[string]int{"number_of_heroes": counter}, w, r)
			return
		case <-r.Context().Done():
			log.Printf("call is canceled")
			return

		case err := <-errorChan:
			log.Printf("While parsing json got error: %+v", err)
			server.BadRequest("json is invalid", err, w, r)
			return

		}
	}
}

// Get hero by ID
func (h HttpServer) GetHero(w http.ResponseWriter, r *http.Request) {
	hero, err := h.service.GetHero(r.Context(), r.URL.Query().Get("id"))
	if err != nil {
		server.RespondWithError(err, w, r)
		return
	}

	response := Hero{
		ID:           hero.ID(),
		Name:         hero.Name(),
		Strength:     hero.Strength(),
		Agility:      hero.Agility(),
		Intelligence: hero.Intelligence(),
		StrGain:      hero.StrGain(),
		AgiGain:      hero.AgiGain(),
		IntGain:      hero.IntGain(),
		Armor:        hero.Armor(),
		Mresist:      hero.Mresist(),
	}

	server.RespondOK(response, w, r)
}
