package transfer

//Hero transport struct
type Hero struct {
	ID           string  `json:"id"`
	Name         string  `json:"name"`
	Strength     int     `json:"strength"`
	Agility      int     `json:"agility"`
	Intelligence int     `json:"intelligence"`
	StrGain      float64 `json:"strength_gain"`
	AgiGain      float64 `json:"agility_gain"`
	IntGain      float64 `json:"intelligence_gain"`
	Armor        float64 `json:"armor"`
	Mresist      float64 `json:"magic_resist"`
}
