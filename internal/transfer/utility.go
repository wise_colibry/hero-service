package transfer

import (
	"context"
	"encoding/json"
	"fmt"
	"hero-service/internal/domain"
	"io"
)

//transfer the hero to http

func heroToDomainViaHttp(h *Hero) (*domain.Hero, error) {
	return domain.NewHero(
		h.ID,
		h.Name,
		h.Strength,
		h.Agility,
		h.Intelligence,
		h.StrGain,
		h.AgiGain,
		h.IntGain,
		h.Armor,
		h.Mresist,
	)
}

// decoder :)
func readHeroes(ctx context.Context, r io.Reader, heroesChan chan Hero) error {

	decoder := json.NewDecoder(r)

	d, err := decoder.Token()
	if err != nil {
		return fmt.Errorf("can not read, with delimiter: %w", err)
	}

	if d != json.Delim('{') {
		return fmt.Errorf("was waiting for {, got  %v", d)
	}

	for decoder.More() {
		if ctx.Err() != nil {
			return ctx.Err()
		}

		x, err := decoder.Token()
		if err != nil {
			return fmt.Errorf("failed to read the hero ID: %w", err)
		}

		heroId, ok := x.(string)

		if !ok {
			return fmt.Errorf("id is not a string, id is  %v", x)
		}

		var hero Hero
		if err := decoder.Decode(&hero); err != nil {
			return fmt.Errorf("decode failed %w", err)
		}

		hero.ID = heroId
		heroesChan <- hero
	}
	return nil
}
