package config

import "os"

// Configutation structure
type Config struct {
	HTTPAddr string
}

//Function to read the conf
func ReadConfig() Config {
	var conf Config
	httpAddr, exists := os.LookupEnv("HTTP_ADDR")
	if exists {
		conf.HTTPAddr = httpAddr
	}
	return conf
}
