package services

import (
	"context"
	"hero-service/internal/domain"
)

// HeroService simple hero service
type HeroRepository interface {
	GetHero(ctx context.Context, id string) (*domain.Hero, error)
	CountHeroes(ctx context.Context) (int, error)
	CreateUpdateTheHero(ctx context.Context, hero *domain.Hero) error
}

// Hero service a simple struct
type HeroService struct {
	repository HeroRepository
}

// Create a new hero service
func NewHeroService(repository HeroRepository) HeroService {
	return HeroService{
		repository: repository,
	}
}

// Return a hero by id
func (h HeroService) GetHero(ctx context.Context, id string) (*domain.Hero, error) {
	return h.repository.GetHero(ctx, id)
}

// Count the total number of heroes
func (h HeroService) CountHeroes(ctx context.Context) (int, error) {
	return h.repository.CountHeroes(ctx)
}

// Update or create a hero
func (h HeroService) CreateUpdateTheHero(ctx context.Context, hero *domain.Hero) error {
	return h.repository.CreateUpdateTheHero(ctx, hero)
}
