package domain

import "errors"

var (
	ErrNil      = errors.New("nil - data is absent")
	ErrNotFound = errors.New("the object is not found")
	ErrRequired = errors.New("value required")
	ErrGopher   = errors.New("not enought gophers for square kilometer")
)
