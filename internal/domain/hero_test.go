package domain

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewHero(t *testing.T) {
	t.Parallel()
	heroName := "Awesome Hero!"
	heroID := "hero id!"

	t.Run("valid hero", func(t *testing.T) {
		hero, err := NewHero(heroID, heroName, 0, 0, 0, 0, 0, 0, 0, 0)
		require.NoError(t, err)
		require.Equal(t, heroID, hero.ID())
		require.Equal(t, heroName, hero.Name())
	})
	t.Run("no hero ID", func(t *testing.T) {
		_, err := NewHero("", heroName, 0, 0, 0, 0, 0, 0, 0, 0)
		require.Error(t, err)
	})

	t.Run("no hero Name", func(t *testing.T) {
		_, err := NewHero(heroID, "", 0, 0, 0, 0, 0, 0, 0, 0)
		require.Error(t, err)
	})

}
