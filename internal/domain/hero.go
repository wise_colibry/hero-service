package domain

import "fmt"

type Hero struct {
	id           string
	name         string
	strength     int
	agility      int
	intelligence int
	strGain      float64
	agiGain      float64
	intGain      float64
	armor        float64
	mResist      float64
}

//constructor for Hero
func NewHero(id, name string, strength, agility, intelligence int, strGain, agiGain, intGain, armor, mResist float64) (*Hero, error) {
	if id == "" {
		return nil, fmt.Errorf("%w: port id is required", ErrRequired)
	}
	if name == "" {
		return nil, fmt.Errorf("%w: port name is required", ErrRequired)
	}

	return &Hero{
		id:           id,
		name:         name,
		strength:     strength,
		agility:      agility,
		intelligence: intelligence,
		strGain:      strGain,
		agiGain:      agiGain,
		intGain:      intGain,
		armor:        armor,
		mResist:      mResist,
	}, nil

}

//Name Setter
func (h *Hero) SetName(name string) error {
	if name == "" {
		return fmt.Errorf("%w: hero name is required", ErrRequired)
	}
	h.name = name
	return nil
}

// all the getters
func (h *Hero) ID() string {
	return h.id
}

func (h *Hero) Name() string {
	return h.name
}

func (h *Hero) Strength() int {
	return h.strength
}
func (h *Hero) Agility() int {
	return h.agility
}
func (h *Hero) Intelligence() int {
	return h.intelligence
}

func (h *Hero) StrGain() float64 {
	return h.strGain
}

func (h *Hero) AgiGain() float64 {
	return h.agiGain
}
func (h *Hero) IntGain() float64 {
	return h.intGain
}
func (h *Hero) Armor() float64 {
	return h.armor
}

func (h *Hero) Mresist() float64 {
	return h.mResist
}
