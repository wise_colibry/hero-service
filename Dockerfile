# small, base image
FROM golang:1.19-alpine AS builder

# Working directory
WORKDIR /app

# Copy go module
COPY go.mod go.sum ./

# download dep
RUN go mod download

COPY . .

# Go binary
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app ./cmd/hero/main.go

# Create a minimal production image
FROM alpine:latest

# Update the package with security patches
RUN apk update && apk upgrade

# Reduce image size
RUN rm -rf /var/cache/apk/* && \
    rm -rf /tmp/*

# No root user running containers :D 
RUN adduser -D herouser 
USER herouser

# Working directory
WORKDIR /app

# only usefull files
COPY --from=builder /app/app .

# Env
ENV HTTP_ADDR=:8080

# Expose the port
EXPOSE 8080

# Run the binary when the container starts
CMD ["./app"]
